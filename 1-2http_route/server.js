var http = require('http');
var url = require('url');
var route = require('./route');
function start() {
	http.createServer(function (request, response) {
		var pathname = url.parse(request.url).pathname;
		route(pathname, request, response);
	}).listen(8888);
	console.log('server is running on port 8888');
}
exports.start = start;