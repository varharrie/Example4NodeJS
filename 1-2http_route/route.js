var exec = require('child_process').exec;

var handle = {};
function route(pathname, request, response) {
	if (typeof handle[pathname] === 'function') {
		handle[pathname](request, response);
	} else {
		response.writeHead(404, { 'Content-Type': 'text/plain' });
		response.write('404 page');
		response.end();
	}
}

handle['/'] = function (request, response) {
	response.writeHead(200, { 'Content-Type': 'text/plain' });
	response.write('index page');
	response.end();
}

handle['/login'] = function (request, response) {
	response.writeHead(200, { 'Content-Type': 'text/plain' });
	response.write('login page');
	response.end();
}

handle['/list'] = function (request, response) {
	exec('dir', function (error, stdout, stderr) {
		response.writeHead(200, { 'Content-Type': 'text/plain' });
		response.write(stdout);
		response.end();
	});
}

module.exports = route;