var http = require('http');
var url = require('url');

function start(router) {
	function onRequest(request, response) {
		var pathname = url.parse(request.url).pathname;
		var data = '';
		request.setEncoding('utf8');
		request.addListener('data', function (dataChunk) {
			data += dataChunk;
		});
		request.addListener('end', function () {
			router.parse(pathname, request, response, data);
		});
	}

	http.createServer(onRequest).listen(8888);
	console.log('server is running on port 8888');
}
exports.start = start;