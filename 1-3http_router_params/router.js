var router = {
	getHandler: [],
	postHandler: []
}
router.parse = function (pathname, request, response, data) {
	if (request.method == 'GET' && typeof this.getHandler[pathname] === 'function') {
		this.getHandler[pathname](request, response);
	} else if (request.method == 'POST' && typeof this.postHandler[pathname] === 'function') {
		this.postHandler[pathname](request, response, data);
	} else {
		response.writeHead(404, { 'Content-Type': 'text/plain' });
		response.write('404 page');
		response.end();
	}
}
router.add = function (method, pathname, handler) {
	switch (method.toUpperCase()) {
		case 'GET':
			this.getHandler[pathname] = handler;
			break;
		case 'POST':
			this.postHandler[pathname] = handler;
			break;
	}
}

module.exports = router;