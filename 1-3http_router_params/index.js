var server = require('./server');
var router = require('./router');

router.add('get', '/login', function (request, response) {
	response.writeHead(200, { 'Content-Type': 'text/html' });
	response.write('<html><head><meta charset="utf-8"/><title>登录页面</title></head><body><form method="post" action="/doLogin"><input name="username" type="text"/><br/><input name="password" type="password"/><br/><input type="submit" value="登录"/></form></body></html>');
	response.end();
});

router.add('post', '/doLogin', function (request, response, data) {
	response.writeHead(200, { 'Content-Type': 'text/html;charset:utf-8' });
	response.write('<h1>' + data + '</h1>');
	response.end();
});

server.start(router);