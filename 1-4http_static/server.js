var http = require('http');
var fs = require('fs');
var path = require('path');
var mime = require('./mime');

var cache = {};

function send404(response) {
	response.writeHead(404, { 'Content-Type': 'text/plain' });
	response.write('Error 404:resource not found.');
	response.end();
}

function sendFile(response, filePath, fileContent) {
	console.log(path.extname(filePath));
	console.log(mime[path.extname(filePath)]);
	response.writeHead(200, { 'Content-Type': mime[path.extname(filePath)] });
	response.end(fileContent);
}

function serveStatic(response, cache, absPath) {
	if (cache[absPath]) {
		sendFile(response, absPath, cache[absPath]);
	} else {
		fs.exists(absPath, function (exists) {
			if (exists) {
				fs.readFile(absPath, function (err, data) {
					if (err) {
						send404();
					} else {
						cache[absPath] = data;
						sendFile(response, absPath, data);
					}
				});
			} else {
				send404(response);
			}
		})
	}
}

var server = http.createServer(function (request, response) {
	var absPath = '';
	if (request.url == '/') {
		absPath = './public/index.html';
	} else {
		absPath = './public' + request.url;
	}
	serveStatic(response, cache, absPath);
});

server.listen(8888, function () {
	console.log('server started on port 8888');
})