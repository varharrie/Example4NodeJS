var express = require('express');
var path = require('path');
var ejs = require('ejs');

var app = express();
var port = process.env.PORT || 8888;

//environments
app.set('port', port);
app.set('views', path.join(__dirname, 'views'));

//使用.ejs
//app.set('view engine', 'ejs');

//使用.html
app.set('view engine','html');
app.engine('html',ejs.__express);

app.use(express.static(path.join(__dirname, 'public')));

app.get('/', function (req, res) {
	res.render('index', { title: 'index' });
});

app.listen(8888, function () {
	console.log('server started on port ' + port);
})