var http=require('http');

//方法一
var server=http.createServer();
server.on('request',function(request,response){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.write('Hello world');
	response.end();
});
server.listen(8888,function(){
	console.log('server started on port 8888');
});

//方法二
/*http.createServer(function(request,response){
	response.writeHead(200,{'Content-Type':'text/plain'});
	response.write('Hello world');
	response.end();
}).listen(8888,function(){
	console.log('server started on port 8888');
});*/